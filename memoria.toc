\babel@toc {spanish}{}
\babel@toc {spanish}{}
\contentsline {chapter}{Dedicatoria}{\es@scroman {i}}% 
\contentsline {chapter}{Agradecimientos}{\es@scroman {ii}}% 
\contentsline {chapter}{Tabla de Contenidos}{\es@scroman {iii}}% 
\contentsline {chapter}{\'{I}ndice de Figuras}{\es@scroman {iv}}% 
\contentsline {chapter}{\'{I}ndice de Tablas}{\es@scroman {v}}% 
\contentsline {chapter}{Tabla de Contenidos}{\es@scroman {vi}}% 
\contentsline {chapter}{Resumen}{\es@scroman {vii}}% 
\contentsline {chapter}{\numberline {1}Introducci\'on}{8}% 
\contentsline {section}{\numberline {1.1}Descripci\'on del Contexto}{8}% 
\contentsline {section}{\numberline {1.2}Definici\'on del Problema}{9}% 
\contentsline {section}{\numberline {1.3}Objetivos}{10}% 
\contentsline {subsection}{\numberline {1.3.1}Objetivo General}{10}% 
\contentsline {subsection}{\numberline {1.3.2}Objetivos Espec\'ificos}{10}% 
\contentsline {section}{\numberline {1.4}Alcances}{10}% 
\contentsline {section}{\numberline {1.5}Propuesta de Soluci\'on}{11}% 
\contentsline {chapter}{\numberline {2}Marco Te\'orico}{12}% 
\contentsline {section}{\numberline {2.1}Tecnolog\'ias Utilizadas}{12}% 
\contentsline {subsection}{\numberline {2.1.1}Frameworks}{12}% 
\contentsline {subsubsection}{ASP.NET Core}{12}% 
\contentsline {subsubsection}{Bootstrap}{13}% 
\contentsline {subsection}{\numberline {2.1.2}Lenguajes}{14}% 
\contentsline {subsubsection}{C\#}{14}% 
\contentsline {subsubsection}{HTML-Razor}{14}% 
\contentsline {subsubsection}{CSS}{14}% 
\contentsline {subsubsection}{JavaScript}{15}% 
\contentsline {subsection}{\numberline {2.1.3}Bibliotecas}{15}% 
\contentsline {subsubsection}{jQuery}{15}% 
\contentsline {section}{\numberline {2.2}Metodolog\'ia Curricular Basada en Competencias}{16}% 
\contentsline {section}{\numberline {2.3}Trabajos Relacionados}{20}% 
\contentsline {subsection}{\numberline {2.3.1}Modelo Curricular del Instruccional Perfomance Technology}{21}% 
\contentsline {subsection}{\numberline {2.3.2}Modelo Curricular TTTI}{23}% 
\contentsline {chapter}{\numberline {3}Construcci\'on del Sistema}{25}% 
\contentsline {section}{\numberline {3.1}Metolodog\'ias}{25}% 
\contentsline {subsection}{\numberline {3.1.1}Metodolog\'ia de Desarrollo: Tradicional Iterativo Incremental}{25}% 
\contentsline {subsection}{\numberline {3.1.2}Metodolog\'ia de evaluaci\'on en Ingenier\'ia de Software: Experimentos Controlados}{27}% 
\contentsline {subsubsection}{Pruebas de Caja Negra}{28}% 
\contentsline {subsubsection}{Pruebas de Caja Blanca}{30}% 
\contentsline {subsection}{\numberline {3.1.3}Pruebas de Usabilidad}{31}% 
\contentsline {chapter}{\numberline {4}Especificaci\'on de Requisitos}{32}% 
\contentsline {section}{\numberline {4.1}Caracter\'isticas de los Usuarios}{33}% 
\contentsline {section}{\numberline {4.2}Requisitos de Usuario}{33}% 
\contentsline {section}{\numberline {4.3}Requisitos de Sistema}{35}% 
\contentsline {section}{\numberline {4.4}Requisitos de Interfaz}{35}% 
\contentsline {section}{\numberline {4.5}Restricciones Generales}{35}% 
\contentsline {subsubsection}{Restricciones de Software}{35}% 
\contentsline {section}{\numberline {4.6}Matriz de Responsabilidades}{36}% 
\contentsline {chapter}{\numberline {5}Dise\~A\pm o del Sistema}{37}% 
\contentsline {section}{\numberline {5.1}Dise\~no Arquitect\'onico}{37}% 
\contentsline {subsection}{\numberline {5.1.1}Arquitectura F\'isica}{37}% 
\contentsline {subsection}{\numberline {5.1.2}Arquitectura L\'ogica}{38}% 
\contentsline {section}{\numberline {5.2}Diagrama de Clases}{41}% 
\contentsline {section}{\numberline {5.3}Diagrama Relacional}{41}% 
\contentsline {section}{\numberline {5.4}Diagramas de Actividades}{41}% 
\contentsline {section}{\numberline {5.5}Diagramas de Casos de Uso}{41}% 
\contentsline {section}{\numberline {5.6}Dise\~no de Interfaz}{41}% 
\contentsline {subsection}{\numberline {5.6.1}Definici\'on de soluci\'on de Dise\~no}{41}% 
\contentsline {subsection}{\numberline {5.6.2}Soluci\'on de Dise\~no}{41}% 
\contentsline {subsubsection}{Teor\'ia del Color}{42}% 
\contentsline {subsubsection}{Tipograf\'ia}{42}% 
\contentsline {subsubsection}{Imagen}{42}% 
\contentsline {subsubsection}{Linea de Dise\~no}{42}% 
\contentsline {subsection}{\numberline {5.6.3}Prototipo de soluci\'on de Dise\~no}{42}% 
\contentsline {chapter}{\numberline {6}Desarrollo}{43}% 
\contentsline {chapter}{\numberline {7}Pruebas y An\'alisis de Resultados}{44}% 
\contentsline {section}{\numberline {7.1}Pruebas de Caja Negra}{44}% 
\contentsline {section}{\numberline {7.2}Pruebas de Caja Blanca}{44}% 
\contentsline {section}{\numberline {7.3}Pruebas de Usabilidad}{44}% 
\contentsline {chapter}{\numberline {8}Conclusiones y Trabajos Futuros}{45}% 
\contentsline {section}{\numberline {8.1}Conclusiones}{45}% 
\contentsline {section}{\numberline {8.2}Trabajo Futuro}{45}% 
\contentsline {chapter}{Glosario}{46}% 
\contentsline {chapter}{Anexos}{}
\contentsline {chapter}{A:\hspace {1em}Marco Te\~A{^3}rico}{49}% 
\contentsline {section}{\numberline {A.1}Diagramas de Metodolog\~A\-a para el Dise\~no Curricular}{49}% 
\contentsline {section}{\numberline {A.2}Plan Curricular Ingenier\~A\-a Civil en Computaci\~A{^3}n}{49}% 
\contentsline {chapter}{B:\hspace {1em}Construcci\~A{^3}n del Sistema}{53}% 
\contentsline {section}{\numberline {B.1}La primera secci\~A{^3}n del segundo anexo}{53}% 
\contentsline {chapter}{C:\hspace {1em}Dise\~no de Software}{54}% 
\contentsline {section}{\numberline {C.1}La primera secci\~A{^3}n del segundo anexo}{54}% 
